module.exports = {
  apps: [
    {
      name: 'bereza_app',
      script: './app.js',
      instances: 1,
      watch: false,
      exec_mode: 'cluster',
      autorestart: false
    }
  ]
}

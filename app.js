import cron from 'node-cron'
import map from './parsers/map.js'
import weather from './parsers/weather.js'
import meteonova from './parsers/meteonova.js'

(async() => {
  try {
    await weather()
    await meteonova()
    await map()
  } catch (err) {
    console.error(err)
  }
})()

cron.schedule('0 0,3,6,9,12,15,18,21 * * *', async() => {
  try {
    await map()
  } catch (err) {
    console.error(err)
  }
})

cron.schedule('0 1,13 * * *', async() => {
  try {
    await weather()
  } catch (err) {
    console.error(err)
  }
})

cron.schedule('0 1 * * *', async() => {
  try {
    await meteonova()
  } catch (err) {
    console.error(err)
  }
})
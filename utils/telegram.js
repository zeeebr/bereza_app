import { Telegraf } from 'telegraf'
import env from './env.js'

class Telegram {
  constructor() {
    this.tg = new Telegraf(env.TELEGRAM_TOKEN)
  }

  async send_screenshot(screenshot, message) {
    await this.tg.telegram.sendPhoto(env.TELEGRAM_CHANNEL, { source: screenshot }, { caption: message })
  }
}

const telegram = new Telegram()

export default telegram

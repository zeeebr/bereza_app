import puppeteer from 'puppeteer-extra'
import StealthPlugin from 'puppeteer-extra-plugin-stealth'
import AdblockerPlugin from 'puppeteer-extra-plugin-adblocker'
import { Telegraf } from 'telegraf'
import env from '../utils/env.js'
import delay from 'delay'
import { DateTime } from 'luxon'

const { telegram } = new Telegraf(env.TELEGRAM_TOKEN)

const SELECTORS = {
  'widgetSelector': 'div[class="widget-body widget-columns-10"]',
  'buttonParameters': 'button[data-stat-value="weather-parameters"]',
  'buttonClose': 'button[class="button back-button js-back"]',
  'selectPollen': 'div[data-value="pollen-birch"]'
}

export default async function weather() {
  let browser
  let page

  try {
    console.log(DateTime.now().plus({ hours: 5 }).toISO(), 'started weather parser!')

    puppeteer.use(AdblockerPlugin())
    puppeteer.use(StealthPlugin())

    browser = await puppeteer.launch({
      headless: (env.isDev) ? false : 'new',
      args: ['--no-sandbox', '--disable-setuid-sandbox']
    })

    page = await browser.newPage()

    await page.setUserAgent('Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/121.0.0.0 Safari/537.36')
    await page.setViewport({ width: 1920, height: 1080 })

    await page.goto('https://www.gismeteo.ru/weather-tyumen-4501/10-days/', { waitUntil: 'domcontentloaded' })

    await page.waitForSelector(SELECTORS.buttonParameters)

    const button = await page.$(SELECTORS.buttonParameters)

    await delay(3000)

    await button.click()

    await page.waitForSelector(SELECTORS.selectPollen)

    const select = await page.$(SELECTORS.selectPollen)

    await delay(3000)

    await select.click()

    const close = await page.$(SELECTORS.buttonClose)

    await close.click()

    await delay(3000)

    const widget = await page.$(SELECTORS.widgetSelector)

    await delay(3000)

    const screenshot = await widget.screenshot()

    await telegram.sendPhoto(env.TELEGRAM_CHANNEL, { source: screenshot })
  } catch (err) {
    if (page) {
      const screenshot = await page.screenshot()
      await telegram.sendPhoto(env.TELEGRAM_CHANNEL, { source: screenshot }, { caption: err.message })
    }

    throw err
  } finally {
    if (browser) {
      await browser.close()
    }
  }
}
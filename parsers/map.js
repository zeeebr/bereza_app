import puppeteer from 'puppeteer-extra'
import StealthPlugin from 'puppeteer-extra-plugin-stealth'
import AdblockerPlugin from 'puppeteer-extra-plugin-adblocker'
import delay from 'delay'
import { Telegraf } from 'telegraf'
import env from '../utils/env.js'
import { DateTime } from 'luxon'

const { telegram } = new Telegraf(env.TELEGRAM_TOKEN)

export default async function map() {
  let browser
  let page

  try {
    console.log(DateTime.now().plus({ hours: 5 }).toISO(), 'started map parser!')

    puppeteer.use(AdblockerPlugin())
    puppeteer.use(StealthPlugin())

    browser = await puppeteer.launch({
      headless: (env.isDev) ? false : 'new',
      args: ['--no-sandbox', '--disable-setuid-sandbox']
    })

    page = await browser.newPage()

    await page.setUserAgent('Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/121.0.0.0 Safari/537.36')
    await page.setViewport({ width: 1920, height: 1080 })

    await page.goto('https://www.gismeteo.ru/maps/allergy/?q=UH%2BRDXtXSqS4zwKKLuBz7Q%3D%3D', { waitUntil: 'domcontentloaded' })

    const mapSelector = 'div[class="interactive-map-container"]'

    await page.waitForSelector(mapSelector)

    await delay(5000)

    const minusSelector = 'div[class="map-point-btn zoom-out"]'

    const minus = await page.$(minusSelector)

    await minus.click()
    await delay(1000)

    await minus.click()
    await delay(1000)

    await minus.click()

    await delay(5000)

    const map = await page.$(mapSelector)

    const screenshot = await map.screenshot()

    await telegram.sendPhoto(env.TELEGRAM_CHANNEL, { source: screenshot })
  } catch (err) {
    if (page) {
      const screenshot = await page.screenshot()
      await telegram.sendPhoto(env.TELEGRAM_CHANNEL, { source: screenshot }, { caption: err.message })
    }

    throw err
  } finally {
    if (browser) {
      await browser.close()
    }
  }
}
import puppeteer from 'puppeteer-extra'
import StealthPlugin from 'puppeteer-extra-plugin-stealth'
import AdblockerPlugin from 'puppeteer-extra-plugin-adblocker'
import delay from 'delay'
import { Telegraf } from 'telegraf'
import { DateTime } from 'luxon'
import env from '../utils/env.js'

const { telegram } = new Telegraf(env.TELEGRAM_TOKEN)

export default async function meteonova() {
  let browser
  let page

  try {
    console.log(DateTime.now().plus({ hours: 5 }).toISO(), 'started meteonova parser!')

    puppeteer.use(AdblockerPlugin())
    puppeteer.use(StealthPlugin())

    browser = await puppeteer.launch({
      headless: (env.isDev) ? false : 'new',
      args: ['--no-sandbox', '--disable-setuid-sandbox']
    })

    page = await browser.newPage()

    await page.setUserAgent('Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/121.0.0.0 Safari/537.36')
    await page.setViewport({ width: 1920, height: 1080 })

    await page.goto('https://www.meteonova.ru/allergy/28367-Tyumen.htm')

    const widgetSelector = 'div[id="frc_content_weather"] > section'

    await page.waitForSelector(widgetSelector)

    await delay(5000)

    const widget = await page.$(widgetSelector)

    const screenshot = await widget.screenshot()

    await telegram.sendPhoto(env.TELEGRAM_CHANNEL, { source: screenshot })
  } catch (err) {
    if (page) {
      const screenshot = await page.screenshot()
      await telegram.sendPhoto(env.TELEGRAM_CHANNEL, { source: screenshot }, { caption: err.message })
    }

    throw err
  } finally {
    if (browser) {
      await browser.close()
    }
  }
}